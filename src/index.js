/*Global process*/
'use strict'
/** 
* Dependencies
* @ignore
*/

const cwd = process.cwd();
const path = require('path');
const express = require('express');
const morgan = require('morgan');
/**
 * Dotenv
 * @ignore
 */
const dotenv = require('dotenv').config();

/**
 * Express
 * @ignore
 */
const {
    PORT: port = 3000
} = process.env;
const app = express();

app.use(morgan('tiny'));
app.use(express.static(path.join(cwd, 'src/build')));

//GET /message
app.get('/message',(req,res) => {
    res.json({
        message: "Woopdi fucking do!"
    });
});

/**
 * LAunch app
 * @ignore
 */

app.listen(port, () => console.log(`Example app listening on port ${port} ${path.join(cwd, 'src/build')}`));