import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
const userUrl = 'https://jsonplaceholder.typicode.com/users/';
const postsUrl = 'https://jsonplaceholder.typicode.com/posts/';
/**
 * Variables
 */

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
class Post {
    constructor(post) {
        this.img = post.img;
        this.id = post.id;
        this.userId = post.userId;
        this.title = post.title;
        this.body = post.body;
    }
}



$(document).ready(function () {
    // TODO Add your listeners here
    //Get all posts
    const xhr = $.ajax({
        url: postsUrl,
        method: 'GET',
        success: function(data) {
            $(data).each((index ,post) => {
                let defaultImg = 'https://studiok40.com/wp-content/uploads/2018/01/open-graph-protocol-logo-article-image.gif';
                post.img = defaultImg;                
                const elPost = createCard(post.img, post.title, post.body, post.id);
                $(elPost).appendTo('.post-elem');  
                const idOfPost = '.post-'+ post.id;
                $(idOfPost).click(() => {
                    showOnlyOnePost(post);
                });
            });
        },
    });
});

/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} title
 * @param {string} body
 * @returns String
 */
function createCard(url, title, body, id) {
    return `
        <div class="col-4 post-${id}">
            <div class="card h-100" style="18rem;">
                ${url
                    ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${url}"/>
                        </div>
                      ` : ''}
                <div class="card-body">
                    <h5>${title}</h5>
                </div>
            </div>
        </div>
    `;
}

function showOnlyOnePost(post) {
    $('.post-elem').hide();
    const detailedPost = detailedCard(post);
    $(detailedPost).appendTo('.detailed-post');
    
}

//Makes the html for a blog post with details
function detailedCard(post) {
    return `
        <div class="col-4" id="${post.id}">
        <div class="card h-100" style="18rem;">
            ${post.img
                ? `
                    <div class="card-img">
                        <img class="card-img-top" src="${post.img}"/>
                    </div>
                ` : ''}
            <div class="card-body">
                <h5>${post.title}</h5>
                ${post.body.split('\n').map(para => `<p>${para}</p>`).join('\n')}

            </div>

        </div>
        </div>
    `;
}